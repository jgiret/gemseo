#
# This file is autogenerated by pip-compile with python 3.10
# To update, run:
#
#    pip-compile --extra=all --extra=test --output-file=requirements/test-python3.10.txt
#
attrs==21.4.0
    # via pytest
certifi==2022.5.18.1
    # via requests
charset-normalizer==2.0.12
    # via requests
coverage[toml]==6.4.1
    # via pytest-cov
cycler==0.11.0
    # via matplotlib
dill==0.3.5.1
    # via openturns
docstring-inheritance==1.0.0
    # via gemseo (setup.py)
et-xmlfile==1.1.0
    # via openpyxl
execnet==1.9.0
    # via pytest-xdist
fastjsonschema==2.15.3
    # via gemseo (setup.py)
fonttools==4.33.3
    # via matplotlib
genson==1.2.2
    # via gemseo (setup.py)
graphviz==0.20
    # via gemseo (setup.py)
h5py==3.6.0
    # via gemseo (setup.py)
idna==3.3
    # via requests
iniconfig==1.1.1
    # via pytest
jinja2==3.1.2
    # via gemseo (setup.py)
joblib==1.1.0
    # via scikit-learn
kiwisolver==1.4.2
    # via matplotlib
markupsafe==2.1.1
    # via jinja2
matplotlib==3.5.2
    # via gemseo (setup.py)
mpmath==1.2.1
    # via sympy
networkx==2.8.2
    # via gemseo (setup.py)
nlopt==2.7.1
    # via gemseo (setup.py)
numpy==1.22.4
    # via
    #   gemseo (setup.py)
    #   h5py
    #   matplotlib
    #   nlopt
    #   pandas
    #   pdfo
    #   pydoe2
    #   pyxdsm
    #   scikit-learn
    #   scipy
openpyxl==3.0.10
    # via gemseo (setup.py)
openturns==1.18
    # via gemseo (setup.py)
packaging==21.3
    # via
    #   gemseo (setup.py)
    #   matplotlib
    #   pytest
pandas==1.4.2
    # via gemseo (setup.py)
pdfo==1.2
    # via gemseo (setup.py)
pillow==9.1.1
    # via matplotlib
pluggy==1.0.0
    # via pytest
psutil==5.9.1
    # via openturns
py==1.11.0
    # via
    #   pytest
    #   pytest-forked
pydoe2==1.3.0
    # via gemseo (setup.py)
pyparsing==3.0.9
    # via
    #   matplotlib
    #   packaging
pyside6==6.3.0
    # via gemseo (setup.py)
pyside6-addons==6.3.0
    # via pyside6
pyside6-essentials==6.3.0
    # via
    #   pyside6
    #   pyside6-addons
pytest==7.1.2
    # via
    #   gemseo (setup.py)
    #   pytest-cov
    #   pytest-forked
    #   pytest-xdist
pytest-cov==3.0.0
    # via gemseo (setup.py)
pytest-forked==1.4.0
    # via pytest-xdist
pytest-xdist==2.5.0
    # via gemseo (setup.py)
python-dateutil==2.8.2
    # via
    #   matplotlib
    #   pandas
pytz==2022.1
    # via pandas
pyxdsm==2.2.1
    # via gemseo (setup.py)
requests==2.27.1
    # via gemseo (setup.py)
scikit-learn==1.1.1
    # via gemseo (setup.py)
scipy==1.7.3
    # via
    #   gemseo (setup.py)
    #   pydoe2
    #   scikit-learn
shiboken6==6.3.0
    # via
    #   pyside6
    #   pyside6-addons
    #   pyside6-essentials
six==1.16.0
    # via python-dateutil
sympy==1.10.1
    # via gemseo (setup.py)
threadpoolctl==3.1.0
    # via scikit-learn
tomli==2.0.1
    # via
    #   coverage
    #   pytest
tqdm==4.64.0
    # via gemseo (setup.py)
typing-extensions==4.2.0
    # via gemseo (setup.py)
urllib3==1.26.9
    # via requests
xdsmjs==1.0.1
    # via gemseo (setup.py)
xxhash==3.0.0
    # via gemseo (setup.py)
